using Gtk;

public class MainWindow : Window {

	/* Setup the initial window */
	public MainWindow () {
		var headerbar = new Gtk.HeaderBar();
		var listContainer = new ListContainer();

		this.window_position = WindowPosition.CENTER;
		setup_headerbar(headerbar);
		add (listContainer);
		set_default_size (800, 600);
		set_titlebar(headerbar);

		this.destroy.connect (Gtk.main_quit);
	}

	private void setup_headerbar (Gtk.HeaderBar headerbar) {
		headerbar.title = "Timer";
		headerbar.subtitle = "Ongoing";
		headerbar.show_close_button = true;
	}


	public static int main (string[] args) {
		Gtk.init (ref args);
		var window = new MainWindow ();
		window.show_all ();

		Gtk.main ();
		return 0;
	}
}

