using Gtk;

public class ListItem : Box {

       	public ListItem (string title, string description, string buttonText) {
		add_row(title, description, buttonText);
       	}

       	private void add_row (string title, string description, string buttonText) {
		Gtk.Grid grid = new Gtk.Grid ();
		Gtk.Button button = new Gtk.Button();
		button.label = buttonText;

		Gtk.Label label = new Gtk.Label (title);
		Gtk.Label label2 = new Gtk.Label (description);

		label.set_hexpand(true);
		label2.set_hexpand(true);

		grid.attach(label, 0, 0, 1, 1);
		grid.attach(label2, 1, 0, 1, 1);
		grid.attach(button, 2, 0, 50, 1);

		set_halign(Gtk.Align.CENTER);

		add (grid);
       	}

}

